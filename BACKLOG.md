# Skillz Backlog
This is the plain-text backlog for the Skillz project.


# Big Hairy Goals
The biggest hairiest goal is: get all Xebia colleagues to use this app.

- Make the app fun to use so everyone will use it
- Make data-entry as easy/quick as possible
- Make sure users get benefits from their work
- Add a level of competition or game dynamics, something like StackOverflow

# Sprint 0

- Enough authentication to allow for people to input their own data. Crowd not really required
- Add skills/interests
- Browse people
- Person details
- Add/change profile info (name, email, etc.). Crowd would help here...
- Browse topics
- Topic details

- Builtin feedback form



# Features

## FEATURE: Authentication against Xebia SSO
    
Allow users to log into Skillz using their normal Xebia SSO accounts, using the Crowd
REST Api to authenticate against

- _Implement basic form-based security in Scalatra_ __DONE__
- _Implement simple in-memory auth service for testing_ __DONE__ 
- Implement remember-me functionality (cookies)
- Implement authentication against Xebia Crowd through REST API
    - install local Crowd and configure app
    - _arrange server space and Crowd access with Thijs_ __IN PROGRESS__ 
- _Add username property to Person_ __DONE__
- _Adjust test data to match_ __DONE__
- _copy UPC ajax handler that detects HTTP 403 errors and redirects to the login page_ __DONE__
- login page error message handling
- add logout button to ui
- use Person as the user class ?
- make logged in user available through thread-local/session facade
- after login-redirect to the profile page of the logged in user
- create new Person for first-time users
    - we will need a post-login event to listen to


## FEATURE: Show a list of people with a one-line stats/summary of their details (IN PROGRESS)
When clicking people in the navigation bar, you should see a list of people arranged
as a stack of bars that show:

- a picture
- the full name
- number of skills
- number of interests
- ?
- List of link icons
    - email
    - skype
    - twitter
    - stackoverflow
    - etc.

The list items link to the person details page

### Add properties to user (DONE)

- add picture url (from twitter or xebia facebook) as a property of person __DONE__
- add StackOverflow link __DONE__
- add Skype ID __DONE__
- add github id __DONE__


## FEATURE: Implement "Add Skills/Interests"

### General Tasks

- Implement server-side API for adding a skill or interest to a person
    - Can you only change your own stuff? The user is already in the session so maybe we should not even put the user id in the URL ?

### Users can add multiple skills and intrerests at once to their own profile
either by clicking the "Add Skill and Interests" button of their own profile bar. 
Users will get an in-place overlayed/hovering editor with the N most popular topics
(and the possibility to filter/search) and two sliders for setting their skill and
interest levels

-   Implement API for retrieving the top N skills not already in your profile
-   Implement multi-topic editor popup
    A popover with a topic and two sliders per line and filled with the top n topics
    and a close/dismiss button (it also closes when clicking on the document outside the popover)
-   Implement profile bar with "Add Skills and Interests" button
-   Implement built-in topic search/filter by name

### Users can add a specific topic to their skills/interests
by clicking on the "Add" button on the topic overview and details pages

-   Implement topic editor popup
    A popover with a topic and two sliders and a close/dismiss button
    (it also closes when clicking on the document outside the popover)
-   Implement "Add this" to the topic overview and details pages
 

### User can update their skills and interests
Reuses the multi-topic editor but now filled with topics you already have in your profile



## FEATURE: Implement user profile/home page where users can edit their own stuff




## FEATURE: Implement "Edit Mode" for current user

When the current user looks at his own profile, all data can be edited. Each individual
field has an edit button beside it that turns text labels into text fields, skills/interests
into topic editors, etc.

    
## FEATURE: Implement "Change skill/interest Level"

- implement server-side API for changing the level on an existing skill/interest
- implement showing of topic editor popup


## FEATURE: Show a list of topics with a one-line stats/summary of their details
When clicking topics in the navigation bar, you should see a list of topics, ordered by number
of people skilled/interested arranged as a stack of bars that show:

- the topic name
- number of skilled
- number of interested

The list items link to the topic details page


## FEATURE: Clicking on a tag will show a list of topics tagged with that tag.
When you click on a tag in the topic details page, you will be taken to a page that show you all the
topics that have an association with that tag. The page will be the same as the normal topic overview
page but the breadcrumbs header will be different.


## FEATURE: Implement Team Builder
Let's say you're looking to put together a team of people of varying skills and you want the most
experienced and motivated people: then you can define a team


## FEATURE: Implement "Activity Streams" on the dashboard i.e. what have people been doing lately
When the user logs in, the dashboard should show a typical modern activity stream, like for example
GitHub or LinkedIn, with information about who now likes or knows what.

### Implementation Note
An activity is nothing else than a relation (IS_SKILLED_AT or IS_INTERESTED_IN) with a creation date. so to show
the last 50 activities, simple walk the graph for all relations with a creation date newer than X and sort by
creation date.

### Ideas
If we support account linking, we could also integrate stackoverflow activities, etc.


# Design

Design more like GitHub for Mac, Sparrow and Twitter for Mac.

- The side bar gives context but should have no sub menus. 
- Use a breadcrumbs like approach on top
    - Breadcrumbs show one level deep, like People/Age Mooij
- Maybe even a twitter for iPad like sliding panels look
- Use iOS-like popovers/popouts

See [this blog about designing GitHub for Mac](http://warpspire.com/posts/designing-github-mac/) for an example of the style



# Implementation details
These are not user-facing features but technical details that need to be implemented

- Store topic tags as Neo4j nodes instead of as a String[] property. Makes searching on tags easier
- Store topic category as Neo4j nodes instead of as a String property. Makes searching on category possible

- implement historic data so we can spots trends
    - store creation date on skills/interests

