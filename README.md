# SKILLZ
A social app to keep track of skills and interests, written using Scala and Neo4j

__Very much a work in progress !__


## The Application
The application will consist of a REST API and a web user interface that talks to it

### REST API
The REST API exposes the following concepts:

- people
- topics
- people know topics with a level of knowledge between 1 - 10
- people like topics with a level between 1 - 10

### WEB User Interface
A web user interface to browse and visualize the above REST API


## Design

- The REST API server is 100% REST-only and does not produce any user interface elements (such as HTML, etc.)
- The web gui talks to the REST API through http and JSON


## Notes

### Tech Used

- Scala 2.9.0-1
- Specs2 for testing
- Scalatra for the REST API
- Maven 3.x (because I couldn't get SBT 0.10.0 to work yet)

### For Eclipse Users
I decided to commit my eclipse project files for this project since the maven eclipse plugin
doesn't yet generate correct .project and .classpath files that support the new 2.x Scala eclipse plugin
