package org.scalatra.auth.strategy

import org.scalatra._
import org.scalatra.auth._


trait FormBasedAuthSupport[UserType <: AnyRef] extends ScentrySupport[UserType] { self: ScalatraKernel =>
    protected def validateLoginCredentials(userName: String, password: String): Option[UserType]
    
    protected def indexPage: String
    protected def loginPage: String
    protected def loginHandler: RouteMatcher
    protected def logoutHandler: RouteMatcher
    
    /**
     * Override this method to add routes that should not be secured. The configured login page
     * and login handler will automatically be marked as non-secure but you might want to mark any
     * images/ css, javascript, etc. used by the login page as unsecured to prevent a redirect loop. */
    def unsecuredRoutes: List[RouteMatcher] = Nil
    
    override protected def registerAuthStrategies {
        scentry.registerStrategy('FormBased, app => new FormBasedAuthStrategy[UserType](app, scentry))
    }
    
    override protected def configureScentry = {
        scentry.unauthenticated {
            scentry.strategies('FormBased).unauthenticated()
        }
    }
    
    /* 
    * Not used unfortunately because it should be overridden by the implementation class this trait
    * will be mixed in with (where you actually know which app you are building and hence what to
    * configure) and the unpredictable initialization order of abstract vals in traits makes it
    * unusable from methods like beforeSome(), get(), post(), etc. since they are called on
    * initialization when vals might not have been set yet. */
    protected val scentryConfig = (new ScentryConfig {}).asInstanceOf[ScentryConfiguration]
    
    /* Adds support for negating a RouteMatcher, as in: !myMatcher or: !"/login.html". */
    implicit private def toRouteMatcherWithNegation[T <% RouteMatcher](routeMatcher: T) = new {
        import org.scalatra.util.MultiMap
        
        def unary_! : RouteMatcher = new RouteMatcher {
            def apply() = {
                routeMatcher() match {
                    case None => Some(MultiMap())
                    case _    => None
                }
            }
            override def toString = "not(" + routeMatcher.toString + ")"
        }
    }
    
    private def securedRoutes: List[RouteMatcher] = {
        "/*" :: !loginPage :: !loginHandler :: unsecuredRoutes.map(!_).toList
    }
    
    
    // ========================================================================
    // Built-in route handlers
    // ========================================================================
    
    beforeSome(securedRoutes:_*) {
        scentry.authenticate('FormBased)
    }

    // TODO: make urls configurable, but string vals don;t work so we have to use an actual RouteMatcher
    post(loginHandler) {
        // TODO make parameter names configurable
        validateLoginCredentials(request.getParameter("username"), request.getParameter("password")) match {
            case Some(authenticatedUser) => {
                user = authenticatedUser.asInstanceOf[UserType]

                redirect(indexPage)
            }
            case None => redirect(loginPage + "?error=Incorrect%20username%20and%2For%20password")
        }
    }
    
    post(logoutHandler) {
        scentry.logout()
        redirect(loginPage + "?message=See%20you%20next%20time!")
    }
}

class FormBasedAuthStrategy[UserType <: AnyRef](val app: ScalatraKernel, val scentry: Scentry[UserType]) extends ScentryStrategy[UserType] {
    def authenticate() = {
        if (scentry.isAuthenticated) {
            // TODO: implement Remember Me using cookies

            scentry.userOption
        } else {
            None
        }
    }

    override def unauthenticated() = {
        if (app.request.isAjax) {
            app.halt(403)
        } else {
            app.redirect("/login.html?Your%20session%20has%20expired.")
            app.halt
        }
    }
    
    /*
    implicit val cookieOptions = CookieOptions(domain = "com.xebia.skillz", maxAge = Int.MaxValue, comment = "Cookie for the Xebia Skillz App")
    // cookies.set("skillz.rememberme", authenticatedUser + "-a444bdc265b6910e5428f2cc8a9386b7566fec21")
    
    response.addHeader(
        "Set-Cookie",
        Cookie("skillz.rememberme", authenticatedUser + "-a444bdc265b6910e5428f2cc8a9386b7566fec21").toCookieString
    )
    */
}


object AuthUtils {
    import javax.servlet.http._
    import scala.collection.JavaConversions._
    
    def dumpRequestInfo(request: HttpServletRequest, params: Map[String, String]) {
        println("============================= REQUEST INFO =====================================")
        
        params.foreach{case (key, value) => println("  Parameter: %s with value: %s".format(key, value))}
        
        println("")
        
        val headerNames: java.util.Enumeration[_] = request.getHeaderNames
        headerNames.foreach( header => println("  Header: " + header + " = " + request.getHeader(header.asInstanceOf[String])))

        println("")

        Option(request.getCookies).getOrElse(Array()).toSeq.foreach{ cookie =>
            println("  Cookie: name: %s, value: %s, domain: %s, path: %s, maxAge: %d"
                        .format(cookie.getName, cookie.getValue, cookie.getDomain, cookie.getPath,cookie.getMaxAge)
            )
        }
        
        println("")
        println("  User in session: " + request.getSession.getAttribute("scentry.auth.default.user"))
        println("================================================================================\n")
    }
}