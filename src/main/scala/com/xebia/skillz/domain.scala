package com.xebia.skillz


case class Tag(
    id: Long = -1,
    name:String
)

case class Topic(
    id: Long = -1,
    name: String, 
    tags: List[Tag] = Nil
)

case class Skill(topic: Topic, level: Int)
case class Interest(topic: Topic, level: Int)

case class Person(
    id: Long = -1,
    name: String,
    username: String,
    email: String,
    skype: Option[String] = None,
    twitter: Option[String] = None,
    stackOverflow: Option[String] = None,
    github: Option[String] = None,
    imageUrl: Option[String] = None,
    skills: List[Skill] = Nil,
    interests: List[Interest] = Nil
)
