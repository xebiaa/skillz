package com.xebia.skillz.graph

import scala.collection.JavaConversions._

import org.neo4j.graphdb._
import org.neo4j.graphdb.index.Index

import com.xebia.skillz._


object Neo4jDomainConverters {
    val INDEX_NAME_TAGS = "tags"
    val INDEX_NAME_TOPICS = "topics"
    val INDEX_NAME_PEOPLE = "people"
    
    val IS_INTERESTED_IN = DynamicRelationshipType.withName("IS_INTERESTED_IN")
    val IS_SKILLED_AT = DynamicRelationshipType.withName("IS_SKILLED_AT")
    val IS_TAGGED_WITH = DynamicRelationshipType.withName("IS_TAGGED_WITH")
    
    
    // ========================================================================
    // Tags
    // ========================================================================
    
    def tagToNode(tag: Tag)(implicit db: GraphDatabaseService): Node = {
        val index = db.index().forNodes(INDEX_NAME_TAGS)
        val node = index.getOrCreateNodeByName(tag.name)

        node.setProperty("type", "TAG")
        node
    }
    
    def nodeToTag(node: Node): Tag = {
        node.getProperty("type") match {
            case "TAG" => Tag(node.getId, node.getProperty("name").asInstanceOf[String])
            case t @ _ => throw new IllegalArgumentException("Cannot create Tag from node because its 'type' property is not 'TAG' but '%s'".format(t))
        }
    }
    
    
    // ========================================================================
    // Topics
    // ========================================================================
    
    def topicToNode(topic: Topic)(implicit db: GraphDatabaseService): Node = {
        val index = db.index().forNodes(INDEX_NAME_TOPICS)
        val node = index.getOrCreateNodeByName(topic.name)

        node.setProperty("type", "TOPIC")

        topic.tags.foreach { (tag) =>
            node.getOrCreateRelation(IS_TAGGED_WITH, tagToNode(tag))
        }

        node
    }

    def nodeToTopic(node: Node): Topic = {
        node.getProperty("type") match {
            case "TOPIC" => Topic(node.getId, node.getProperty("name").asInstanceOf[String], tags(node))
            case t @ _   => throw new IllegalArgumentException("Cannot create Topic from node because its 'type' property is not 'TOPIC' but '%s'".format(t))
        }
    }
    
    private def tags(topic: Node): List[Tag] = {
        topic.getRelationships(IS_TAGGED_WITH)
              .view
              .map(relation => nodeToTag(relation.getEndNode))
              .toList
              .sortBy(_.name)
    }
    
    private object SimpleTopicConverters {
        implicit def simpleTopicToNode(topic: Topic)(implicit db: GraphDatabaseService): Node = topicToNode(topic)(db)
        implicit def nodeToSimpleTopic(node: Node): Topic = nodeToTopic(node)
    }
    
    
    // ========================================================================
    // People
    // ========================================================================
    
    def personToNode(person: Person)(implicit db: GraphDatabaseService): Node = {
        val index = db.index().forNodes(INDEX_NAME_PEOPLE)
        val node = index.getOrCreateNodeByProperty("username", person.username)

        node.setProperty("type", "PERSON")
        node.setProperty("name", person.name)
        node.setProperty("email", person.email)
        
        optionToNodeProperty(person.skype, node, "skype")
        optionToNodeProperty(person.twitter, node, "twitter")
        optionToNodeProperty(person.github, node, "github")
        optionToNodeProperty(person.stackOverflow, node, "stackOverflow")
        optionToNodeProperty(person.imageUrl, node, "imageUrl")

        index.add(node, "name", person.name.toLowerCase)

        import SimpleTopicConverters._

        person.skills.foreach { (skill) =>
            node.getOrCreateRelation(IS_SKILLED_AT, skill.topic).setProperty("level", skill.level)
        }

        person.interests.foreach { (interest) =>
            node.getOrCreateRelation(IS_INTERESTED_IN, interest.topic).setProperty("level", interest.level)
        }

        node
    }
    
    def nodeToPerson(node: Node): Person = {
        node.getProperty("type") match {
            case "PERSON" => Person(id = node.getId,
                                    name = node.getProperty("name").asInstanceOf[String],
                                    username = node.getProperty("username").asInstanceOf[String],
                                    email = node.getProperty("email").asInstanceOf[String],
                                    skype = stringPropertyToOption(node, "skype"),
                                    twitter = stringPropertyToOption(node, "twitter"),
                                    github = stringPropertyToOption(node, "github"),
                                    stackOverflow = stringPropertyToOption(node, "stackOverflow"),
                                    imageUrl = stringPropertyToOption(node, "imageUrl"),
                                    skills = skills(node),
                                    interests = interests(node))
            case t @ _    => throw new IllegalArgumentException("Cannot create Person from node because its 'type' property is not 'PERSON' but '%s'".format(t))
        }
    }
    
    private def stringPropertyToOption(node: Node, propertyName: String): Option[String] = {
        node.getProperty(propertyName, null) match {
            case null => None
            case value @ _ => Some(value.asInstanceOf[String])
        }
    }
    
    private def optionToNodeProperty(option: Option[String], node: Node, propertyName: String) {
        option match {
            case Some(value) => node.setProperty(propertyName, value)
            case None =>
        }
    }

    private def skills(person: Node): List[Skill] = {
        import SimpleTopicConverters._
        person.getRelationships(IS_SKILLED_AT)
              .view
              .map(relation => Skill(relation.getEndNode, relation.getProperty("level").asInstanceOf[Int]))
              .toList
              .sortBy(_.level)(implicitly[Ordering[Int]].reverse)
    }

    private def interests(person: Node): List[Interest] = {
        import SimpleTopicConverters._
        person.getRelationships(IS_INTERESTED_IN)
              .view
              .map(relation => Interest(relation.getEndNode, relation.getProperty("level").asInstanceOf[Int]))
              .toList
              .sortBy(_.level)(implicitly[Ordering[Int]].reverse)
    }
}
