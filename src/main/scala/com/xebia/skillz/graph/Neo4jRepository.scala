package com.xebia.skillz
package graph

import scala.collection.JavaConversions._
import scala.collection.immutable.ListMap
import scala.util.control.Exception._

import org.neo4j.graphdb._

import com.xebia.skillz._
import Neo4jDomainConverters._
        

class Neo4jRepository(graphDb: GraphDatabaseService) {
    implicit val db = graphDb
    
    val peopleIndex = db.index().forNodes(INDEX_NAME_PEOPLE)
    val topicIndex = db.index().forNodes(INDEX_NAME_TOPICS)
    val tagIndex = db.index().forNodes(INDEX_NAME_TAGS)


    // ========================================================================
    // People
    // ========================================================================

    def people: List[Person] = peopleMatching("")
    def peopleMatching(prefix: String): List[Person] = {
        peopleIndex.query("name", prefix.toLowerCase + "*").iterator.map(nodeToPerson(_)).toList
    }
    
    def person(id: Long): Option[Person] = catching(classOf[NotFoundException]) opt {
        nodeToPerson(db.getNodeById(id))
    }

    def personByUsername(username: String): Option[Person] = {
        personByIndexedProperty("username", username)
    }
    
    def personByName(name: String): Option[Person] = {
        personByIndexedProperty("name", name)
    }
    
    private[graph] def personByIndexedProperty(propertyName: String, propertyValue: String): Option[Person] = {
        catching(classOf[NoSuchElementException]) opt {
            peopleIndex.get(propertyName, propertyValue.toLowerCase).getSingle match {
                case node: Node => nodeToPerson(node)
                case _          => throw new NoSuchElementException()
            }
        }
    }

    def skilledAt(topic: Topic): Map[Person, Skill] = {
        val skilled = db.getNodeById(topic.id).getRelationships(IS_SKILLED_AT, Direction.INCOMING).map { relation =>
            (nodeToPerson(relation.getStartNode), Skill(topic, relation.getProperty("level").asInstanceOf[Int]))
        }
        
        ListMap(skilled.view.toList.sortBy(_._2.level)(implicitly[Ordering[Int]].reverse):_*)
    }
    
    def interestedIn(topic: Topic): Map[Person, Interest] = {
        val interested = db.getNodeById(topic.id).getRelationships(IS_INTERESTED_IN, Direction.INCOMING).map { relation =>
            (nodeToPerson(relation.getStartNode), Interest(topic, relation.getProperty("level").asInstanceOf[Int]))
        }
        
        ListMap(interested.view.toList.sortBy(_._2.level)(implicitly[Ordering[Int]].reverse):_*)
    }
    

    // ========================================================================
    // Topics
    // ========================================================================
    
    def topics: List[Topic] = topicsMatching("")
    def topicsMatching(prefix: String): List[Topic] = {
        topicIndex.query("name", prefix.toLowerCase + "*").iterator.map(nodeToTopic(_)).toList
    }
    
    def topic(id: Long): Option[Topic] = catching(classOf[NotFoundException]) opt {
        nodeToTopic(db.getNodeById(id))
    }
    
    def topicNamed(name: String): Option[Topic] = {
        catching(classOf[NoSuchElementException]) opt {
            topicIndex.get("name", name.toLowerCase).getSingle match {
                case node: Node => nodeToTopic(node)
                case _          => throw new NoSuchElementException()
            }
        }
    }
    
    
    // ========================================================================
    // Tags
    // ========================================================================
    
    def tags: List[Tag] = tagIndex.query("name", "*").iterator.map(nodeToTag(_)).toList
    
    def tagByName(name: String): Option[Tag] = {
        catching(classOf[NoSuchElementException]) opt {
            tagIndex.get("name", name.toLowerCase).getSingle match {
                case node: Node => nodeToTag(node)
                case _          => throw new NoSuchElementException()
            }
        }
    }
    
    def topicsByTag(tagName: String): List[Topic] = tagByName(tagName) match {
        case Some(tag) => topicsByTag(tag)
        case None => List[Topic]()
    }
    
    def topicsByTag(tag: Tag): List[Topic] = {
        db.getNodeById(tag.id)
          .getRelationships(IS_TAGGED_WITH, Direction.INCOMING)
          .view
          .map(relation => nodeToTopic(relation.getStartNode))
          .toList
    }
}


trait Neo4jTestData {
    // Note: using this instead of a self type annotation (self: Neo4jRepository =>) because of a
    // known bug in Scala 2.9 (see https://issues.scala-lang.org/browse/SI-4560)
    implicit val db: GraphDatabaseService
    
    def string2Tag(string: String) = Tag(name = string)
    
    val java       = Topic(name = "Java",       tags = List("Programming Language", "JVM-based", "Object Oriented", "Statically Typed", "JVM", "JEE", "J2EE", "JSE", "J2SE").map(string2Tag(_)))
    val scala      = Topic(name = "Scala",      tags = List("Programming Language", "JVM-based", "Functional", "Object Oriented", "Statically Typed").map(string2Tag(_)))
    val javascript = Topic(name = "Javascript", tags = List("Programming Language", "Functional", "Dynamically Typed", "Browser", "Node", "V8", "SpiderMonkey").map(string2Tag(_)))
    val haskell    = Topic(name = "Haskell",    tags = List("Programming Language", "Functional", "Pure", "Academic", "Statically Typed", "Monad").map(string2Tag(_)))
    val mongoDB    = Topic(name = "MongoDB",    tags = List("Database", "Document Store", "NoSQL", "JSON", "BSON").map(string2Tag(_)))
    val neo4j      = Topic(name = "Neo4j",      tags = List("Database", "Graph Database", "NoSQL", "Java").map(string2Tag(_)))
    val iOS        = Topic(name = "iOS",        tags = List("Mobile Platform", "Apple", "mobile", "iOS4", "iOS5", "iPhone", "iPad").map(string2Tag(_)))
    val android    = Topic(name = "Android",    tags = List("Mobile Platform", "Google", "mobile").map(string2Tag(_)))
    
    val age = Person(
        name = "Age Mooij",
        username = "amooij",
        email = "amooij@xebia.com",
        skype = Some("agemooy"),
        twitter = Some("agemooij"),
        github = Some("agemooij"),
        stackOverflow = Some("agemooij"),
        imageUrl = Some("http://localhost:8080/assets/people/amooij.png"),
        skills = List(Skill(scala, 8),
                      Skill(java, 10),
                      Skill(javascript, 7),
                      Skill(mongoDB, 7),
                      Skill(neo4j, 6),
                      Skill(iOS, 6)),
        interests = List(Interest(scala, 10),
                         Interest(java, 6),
                         Interest(javascript, 8),
                         Interest(mongoDB, 10),
                         Interest(neo4j, 9),
                         Interest(iOS, 8)))
    
    val bram = Person(
        name = "Bram Neijt",
        username = "bneijt",
        email = "bneijt@xebia.com",
        skype = Some("bneijt"),
        github = Some("bneijt"),
        stackOverflow = Some("bneijt"),
        imageUrl = Some("http://localhost:8080/assets/people/bneijt.png"),
        skills = List(Skill(java, 6),
                      Skill(haskell, 7),
                      Skill(javascript, 7)),
        interests = List(Interest(java, 6),
                         Interest(haskell, 9)))
                         
     val freek = Person(
         name = "Freek Wielstra",
         username = "fwielstra",
         email = "fwielstra@xebia.com",
         skype = Some("freek.wielstra"),
         github = Some("fwielstra"),
         stackOverflow = Some("Cthulhu"),
         imageUrl = Some("http://localhost:8080/assets/people/fwielstra.png"),
         skills = List(Skill(java, 7),
                       Skill(javascript, 8)),
         interests = List(Interest(java, 7),
                          Interest(javascript, 9)))
    
    db.withTransaction {
        List(java, javascript, scala, haskell, mongoDB, neo4j, iOS, android).map(topicToNode(_))
        List(age, bram, freek).map(personToNode(_))
    }
}
