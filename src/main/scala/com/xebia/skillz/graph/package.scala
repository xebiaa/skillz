package com.xebia.skillz

import java.io.File

import scala.collection.JavaConversions._

import org.neo4j.kernel.EmbeddedGraphDatabase
import org.neo4j.graphdb._
import org.neo4j.graphdb.index.Index


package object graph {
    def createEmbeddedDatabase(path: String = createTempDatabaseDir().getAbsolutePath()) = new EmbeddedGraphDatabase(path)

    private def createTempDatabaseDir() = {
        val dir = File.createTempFile("skillz-neo4j-db-", "");

        println(String.format("Created a new embedded Neo4j database at [%s]", dir.getAbsolutePath()));

        if (!dir.delete()) {
            throw new RuntimeException("temp config directory pre-delete failed");
        }

        if (!dir.mkdirs()) {
            throw new RuntimeException("temp config directory not created");
        }

        dir
    }


    // ========================================================================
    // Neo4j API Pimping
    // ========================================================================

    implicit def toPimpedNode(node: Node) = new {
        def getOrCreateRelation(relationType: RelationshipType, endNode: Node): Relationship = {
            node.getRelationships(relationType).find(_.getEndNode == endNode) match {
                case Some(relation) => relation
                case None => node.createRelationshipTo(endNode, relationType)
            }
        }
    }

    implicit def toPimpedPropertyContainer(propertyContainer: PropertyContainer) = new {
        def apply(property: String): Option[Any] = {
            propertyContainer.getProperty(property) match {
                case null   => None
                case p: Any => Some(p)
            }
        }

        def update(property: String, value: Any): Unit = propertyContainer.setProperty(property, value)
    }

    implicit def toPimpedNodeIndex(index: Index[Node]) = new {
        def getOrCreateNodeByName(name: String)(implicit db: GraphDatabaseService): Node = getOrCreateNodeByProperty("name", name)
        def getOrCreateNodeByProperty(name: String, value: String)(implicit db: GraphDatabaseService): Node = {
            index.get(name, value.toLowerCase).getSingle() match {
                case n: Node => n
                case _ => {
                    val newNode = db.createNode()

                    newNode.setProperty(name, value)
                    index.add(newNode, name, value.toLowerCase)

                    newNode
                }
            }
        }
    }

    implicit def toPimpedDatabase(db: GraphDatabaseService) = new {
        def withTransaction(f: => Unit) {
            val tx = db.beginTx

            try {
                f
                tx.success
            }
            finally {
                tx.finish
            }
        }
    }
}
