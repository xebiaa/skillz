package com.xebia.skillz.server

import net.liftweb.json._
import net.liftweb.json.JsonAST.JArray
import net.liftweb.json.JsonDSL._

import com.xebia.skillz._


object JsonDomainConverters {
    def personToJValue(person: Person) = {
        ("id" -> person.id) ~
        ("name" -> person.name) ~
        ("email" -> person.email) ~
        ("skype" -> person.skype) ~
        ("twitter" -> person.twitter) ~
        ("github" -> person.github) ~
        ("stackOverflow" -> person.stackOverflow) ~
        ("imageUrl" -> person.imageUrl)
    }
    
    implicit def peopleToJArray(people: List[Person]) = {
        new JArray(people.map(person => 
            personToJValue(person) ~ 
            ("nrOfSkills" -> person.skills.size) ~ 
            ("nrOfInterests" -> person.interests.size)))
    }
    
    // TODO: combine these two using a shared trait or structural type
    implicit def skillToJValue(skill: Skill) = ("id" -> skill.topic.id) ~ ("name" -> skill.topic.name) ~ ("level" -> skill.level)
    implicit def interestToJValue(interest: Interest) = ("id" -> interest.topic.id) ~ ("name" -> interest.topic.name) ~ ("level" -> interest.level)
    
    implicit def tagToJValue(tag: Tag) = JString(tag.name)
    
    implicit def personToJValueWithAllProperties(person: Person) = { //personToJValueWithAllProperties(person)
        personToJValue(person) ~
        ("skills" -> person.skills) ~
        ("interests" -> person.interests)
    }

    def topicToJValue(topic: Topic) = {
        ("id" -> topic.id) ~
        ("name" -> topic.name) ~
        ("tags" -> topic.tags)
    }
    
    implicit def topicsToJArray(topics: List[Topic]) = new JArray(topics.map(topicToJValue(_)))

    def topicToJValueWithAllProperties(topic: Topic, skilledAt: Map[Person, Skill], interestedIn: Map[Person, Interest]) = {
        topicToJValue(topic) ~
        ("skilledAt" -> skilledAt.map{ case(person, skill) =>
            ("id" -> person.id) ~ ("name" -> person.name) ~ ("email" -> person.email) ~ ("level" -> skill.level)
        }) ~
        ("interestedIn" -> interestedIn.map{ case(person, interest) =>
            ("id" -> person.id) ~ ("name" -> person.name) ~ ("email" -> person.email) ~ ("level" -> interest.level)
        })
    }
}