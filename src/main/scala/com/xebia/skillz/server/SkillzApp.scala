package com.xebia.skillz.server

import org.scalatra._


class SkillzApp extends ScalatraFilter with SkillzRestApi {
    protected def indexPage = "/"
    protected def loginPage = "/login.html"
    protected def loginHandler = "/auth/login"
    protected def logoutHandler = "/auth/logout"
    
    override def unsecuredRoutes: List[RouteMatcher] = List(
        "/favicon.ico",
        "/assets/*",
        "/css/*",
        "/js/*",
        "/plugins/*",
        "/templates/*"
    )
}