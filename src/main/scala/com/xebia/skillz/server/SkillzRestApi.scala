package com.xebia.skillz.server

import scala.util.control.Exception._

import net.liftweb.json._
import net.liftweb.json.JsonDSL._

import org.scalatra.ScalatraKernel
import org.scalatra.auth.strategy._

import com.xebia.skillz._
import com.xebia.skillz.graph._
import JsonDomainConverters._


case class SkillzUser(username: String)

trait SkillzRestApi extends FormBasedAuthSupport[SkillzUser] { self: ScalatraKernel =>
    protected implicit val db = createEmbeddedDatabase()
    protected val repository = new Neo4jRepository(db) with Neo4jTestData
    
    // ========================================================================
    // Authentication Support
    // ========================================================================
    
    protected def fromSession = {case username: String => SkillzUser(username)}
    protected def toSession = {case usr: SkillzUser => usr.username}
    
    protected def validateLoginCredentials(username: String, password: String) = {
        // TODO: replace this excellent authentication with something a little bit more complex...
        List("amooij", "fwielstra", "bneijt", "gbossuyt").contains(username.toLowerCase) match {
            case true if username.toLowerCase == password.toLowerCase => Some(SkillzUser(username))
            case _ => None
        }
    }
    
    // TODO: handle initial login for users that have no associated Person yet. Can we retrieve al the necessary info from Crowd?
    
    private def currentUserAsPerson: Option[Person] = {
        // userOption match {
        //     case Some(loggedInUser) => repository.personByUsername(loggedInUser.username)
        //     case None => throw new IllegalStateException("There is no current user so you should not call the currentUserAsPerson method.")
        // }
        
        userOption.flatMap(loggedInUser => repository.personByUsername(loggedInUser.username))
    }


    // ========================================================================
    // People
    // ========================================================================

    get("/api/1/people") {
        json(repository.people)
    }
    
    get("/api/1/people/suggest/:prefix") {
        json(repository.peopleMatching(params("prefix")))
    }
    
    get("/api/1/people/:id") {
        repository.person(paramAsLong("id")) match {
            case Some(person) => json(person)
            case None         => halt(404, json(("error" -> "A person with id %s could not be found.".format(params("id")))))
        }
    }
    
    post("/api/1/people/:id/skills") {
        
        
        
        // INPUT: {topic: 42, level: 8}
        halt(404, json(("error" -> "Create/Update is not implemented yet for skills.")))
    }
    
    post("/api/1/people/:id/interests") {
        // INPUT: {topic: 37, level: 8}
        halt(404, json(("error" -> "Create/Update is not implemented yet for interests.")))
    }

    // ========================================================================
    // Topics
    // ========================================================================

    get("/api/1/topics") {
        json(repository.topics)
    }
    
    get("/api/1/topics/suggest/:prefix") {
        json(repository.topicsMatching(params("prefix")))
    }
    
    get("/api/1/tags") {
        json(repository.tags)
    }
    
    get("/api/1/tags/:name/topics") {
        json(repository.topicsByTag(params("name")))
    }

    get("/api/1/topics/:id") {
        repository.topic(paramAsLong("id")) match {
            case None        => halt(404, json(("error" -> "A person with id %s could not be found.".format(params("id")))))
            case Some(topic) => {
                json(topicToJValueWithAllProperties(topic, repository.skilledAt(topic), repository.interestedIn(topic)))
            }
        }
    }
    
    post("/api/1/topics") {
        halt(404, json(("error" -> "Create/Update is not implemented yet for topics.")))
    }
    
    post("/api/1/topics/:id/tags") {
        // INPUT: {tags: ['bla', 'doh']}
        halt(404, json(("error" -> "Create/Update is not implemented yet for topic tags.")))
    }
    
    
    // ========================================================================
    // Misc helpers
    // ========================================================================
    
    private def paramAsLong(name: String) = catching(classOf[NumberFormatException]) opt params(name).toLong getOrElse(-1L)

    private def json(value: JValue): String = {
        contentType = "application/json"
        
        compact(render(value))
    }
}
