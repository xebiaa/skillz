var Dashboard = (function(Backbone) {
    var DashboardController = Backbone.Controller.extend({
        initialize : function() {
            this.view = new DashboardView();
            
            this.route("dashboard", "dashboard", function() {
                this.view.render();
            })
            
            console.log("DashboardController initialized.")
        }
    });

    var DashboardView = Backbone.View.extend({
        render: function() {
            Navigation.activateMenu("dashboard");
            
            Template.renderIntoMain("dashboard");
        }
    });
    
    return {
        Controller: DashboardController,
        View: DashboardView
    };
}(Backbone));
