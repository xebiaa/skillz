var Navigation = (function() {
    return {
        activateMenu: function(id) {
            var nav = $("ul#navigation")
            
            nav.find("li.current").removeClass('current');
            nav.find('li a.current').removeClass('current');
            
            nav.find("li#" + id).addClass('current');
            nav.find("li#" + id + " a").addClass('current');
        }
    }
}());