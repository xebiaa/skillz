var People = (function(Backbone) {
    var PeopleController = Backbone.Controller.extend({
        initialize : function() {
            this.view = new PeopleView()
            
            this.route("people", "people", function() {
                this.view.renderPeople();
            })
        
            this.route("person/:id", "person", function(id) {
                this.view.renderPerson(id)
            })
            
            console.log("PeopleController initialized.")
        }
    });
  
    var PeopleView = Backbone.View.extend({
        renderPeople: function() {
            Navigation.activateMenu("people")
            
            $.ajax({
                url: "/api/1/people",
                dataType: 'json',
                success: function(people) {
                    
                    _(people).each(function(element, index) {
                        element.class = (index % 2 == 0) ? "even" : "odd";
                    });
                    
                    Template.renderIntoMain("people", {"people": people});
                },
                error: function(xhr, status, error) {
                    console.log("Error while getting the list of all people");
                    console.log("  Status: " + status);
                    console.log("  Error: " + error);
                }
            });
        },
        
        renderPerson: function(id) {
            Navigation.activateMenu("people")
            
            var addPercentage = function(skillOrInterest) {
                // add extra "percentage" property to skills/interests needed for display purposes only
                skillOrInterest.percentage = skillOrInterest.level * 10;
                
                return skillOrInterest;
            }
            
            var processAndRender = function(details) {
                details.skills = _(details.skills).map(addPercentage)
                details.interests = _(details.interests).map(addPercentage)
    
                Template.renderIntoMain("person", details);
            }
            
            $.ajax({
                url: "/api/1/people/" + id,
                dataType: 'json',
                success: processAndRender,
                error: function(xhr, status, error) {
                    console.log("Error while getting details for person with id " + id);
                    console.log("  Status: " + status)
                    console.log("  Error: " + JSON.parse(error).error)
                }
            });
        }
    });
  
    return {
        Controller: PeopleController,
        View: PeopleView
    };

}(Backbone));