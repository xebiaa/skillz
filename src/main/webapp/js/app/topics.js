var Topics = (function(Backbone) {
    var TopicsController = Backbone.Controller.extend({
        initialize : function() {
            this.view = new TopicsView()
            
            this.route("topics", "topics", function() {
                this.view.renderTopics();
            })
        
            this.route("topic/:id", "topic", function(id) {
                this.view.renderTopic(id)
            })
            
            // this.route("topics/with/tag/:tag")
            
            console.log("TopicsController initialized.")
        }
    });
  
    var TopicsView = Backbone.View.extend({
        renderTopics: function() {
            Navigation.activateMenu("topics")
            
            Template.renderIntoMain("dashboard");
        },
        
        renderTopic: function(id) {
            Navigation.activateMenu("topics")
            
            var hardcodedDetails = {
                "id":2,
                "name":"Javascript",
                "category":"Programming Language",
                "tags":["Functional", "Prototypical", "Node"],
                "skilledAt":[
                    {"id":9,"name":"Age Mooij","level":7},
                    {"id":10,"name":"Bram Neijt","level":7},
                    {"id":11,"name":"Freek Wielstra","level":8}
                ],
                "interestedIn":[
                    {"id":9,"name":"Age Mooij","level":8},
                    {"id":11,"name":"Freek Wielstra","level":9}
                ]
            }
            
            var addPercentage = function(skillOrInterest) {
                // add extra "percentage" property to skills/interests needed for display purposes only
                skillOrInterest.percentage = skillOrInterest.level * 10; return skillOrInterest;
            }
            
            var processAndRender = function(details) {
                details.skilledAt = _(details.skilledAt).map(addPercentage)
                details.interestedIn = _(details.interestedIn).map(addPercentage)
    
                Template.renderIntoMain("topic", details);
            }
            
            $.ajax({
                url: "/api/1/topics/" + id,
                dataType: 'json',
                success: processAndRender,
                error: function(xhr, status, error) {
                    console.log("Error while getting details for topic with id " + id);
                    console.log("  Status: " + status)
                    console.log("  Error: " + JSON.parse(error).error)
                    
                    processAndRender(hardcodedDetails);
                }
            });
        }
    });
  
    // export publically accessible items here.
    return {
        Controller: TopicsController,
        View: TopicsView
    };
  
}(Backbone));
