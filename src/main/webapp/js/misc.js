
$(document).ready(function() {
    
    // ============================================================================
    // jQuery customizations
    // ============================================================================

    $(document).ajaxError(function(event, xhr, ajaxOptions) {
    	switch(xhr.status) {
    	case 403:
            console.log("Ajax call returned with a 403 status, meaning our session had expired. Redirecting to the login page...");

            window.location.href = "/login.html?error=Your%20session%20has%20expired.";
            break;
    	case 500:
    		alert("An error occurred, please contact the administrator.");
    		break;
    	}
    });    

    // ============================================================================
    // Customized GUI behaviour
    // Adjusted from the original version in the yourAdmin templates
    // ============================================================================    
    
    // Box visibility toggling
    // Triggers on a click on the toggle icon or the box title
    $('.toggle, .box .header h2 a').live('click', function(e) {
        e.preventDefault();
        var $boxparent = $($(this).closest('.box'));
        // show or hide depending on current state.
        if ($boxparent.hasClass("hidden")) {
            $boxparent.removeClass("hidden");
            $boxparent.children('.content').slideDown();
        } else {
            $boxparent.addClass("hidden");
            $boxparent.children('.content').slideUp();
        }
    });

    // Tabbed content
    $('div.tabs').livequery(function() {
        $(this).each(function(index) {
            var $container = $(this);
            var $tabContainers = $(this).children('.tab')

            $tabContainers.hide();
            $tabContainers.filter(':first').show().addClass("active"); // show first tab in list and set to active

            $('ul.navigation li', this).click(function() {
                $container.parent().find('li a.current').removeClass("current");
                $('a', this).addClass("current");
                $container.find('.tab.active').hide().removeClass("active");
                var activeTab = $(this).find("a").attr("href");
                $(activeTab, $container).fadeIn().addClass("active");
                return false;
            });
        });
    });

    // Accordion
    $('div.accordion').livequery(function() {
        $(this).each(function(index) {
            var trigger = $(this).children('.panel');
            var accordionContainer = $(this).children('.content');
            accordionContainer.hide();

            accordionContainer.filter(':first').show();
            trigger.filter(':first').addClass('active');
            trigger.click(function(e) {
                e.preventDefault();
                if ($(this).next().is(':hidden')) {
                    trigger.removeClass('active').next().slideUp();
                    $(this).toggleClass('active').next().slideDown();
                }
                return false;
            });
        });
    });

    // Messages
    $('.message .close').live('click', function() {
        $(this).parent().fadeOut();
    });

    // Child $this
    if (!$('body').hasClass('altlayout')) {
        $("#navigation").superfish({
            speed : 'fast',
            delay : 300,
            autoArrows : false
        });
    }

    // jQuery Facebox Modal
    $('a[rel*=modal]').livequery(function() {
        $(this).facebox({
            opacity : 0.6
        });
    });

    // jQuery Datables
    $('.datatable').livequery(function() {
        $(this).dataTable({
            "sPaginationType" : "full_numbers",
            "iDisplayLength": 25
        })
    });

    // JQuery Uniform
    $("select, input[type='checkbox'], input[type='radio'], input[type='file']").livequery(function() {
        $(this).uniform();
    });

    // jQuery Tipsy
    $('[rel=tooltip]').livequery(function() {
        $(this).tipsy({
            gravity : 's',
            fade : true
        }); // Tooltip Gravity Orientation: n | w | e | s
    });

    // jQuery Superfish
    $("#menu").livequery(function() {
        $(this).superfish({
            speed : 'fast',
            delay : 300,
            autoArrows : false
        });
    });

});
