var Template = (function() {
    return {
        // adds the template located at /templates/name.html to icanhaz, calling
        // the callback when loaded.
        loadTemplate: function(name, callback) {
            if (ich.templates[name]) {
                // template already loaded.
                callback();
            } else {
                $.get('/templates/' + name + '.html', function(template) {
                    ich.addTemplate(name, template);
                    callback();
                });
            }
        },
        // loads all the templates in the given array, calling the callback
        // when all templates were loaded.
        loadTemplates: function(templateNames, callback) {
            var countdown = templateNames.length;
            for (var i = 0; i < templateNames.length; i++) {
                this.loadTemplate(templateNames[i], function() {
                    // one template loaded, we done yet?
                    if (--countdown == 0) {
                        callback();
                    }
                });
            }
        },

        // Load the given template and render it into an element matching 
        // selector '#main'.
        renderIntoMain: function(templateName, data) {
            this.loadTemplate(templateName, function() {
               $('#main').html(ich[templateName](data)); 
            });
        }
    };
}());