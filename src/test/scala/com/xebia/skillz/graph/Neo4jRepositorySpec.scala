package com.xebia.skillz.graph

import org.neo4j.graphdb._

import org.specs2.mutable._

import com.xebia.skillz._
import com.xebia.skillz.graph._
import com.xebia.skillz.graph.Neo4jDomainConverters._


class Neo4jRepositorySpec extends SpecificationWithJUnit {
    val repository = new Neo4jRepository(createEmbeddedDatabase()) with Neo4jTestData
    
    "A Neo4jRepository loaded with people from the standard test data set" should {
        "return all people from the database" in {
            val people = repository.people
            
            people must have size(3)
            people.map(_.name) must contain("Age Mooij", "Bram Neijt", "Freek Wielstra")
        }
        
        "return an empty List[Person] for people whose name begins with 'Z'" in {
            repository.peopleMatching("Z") must have size(0)
        }
        
        "return a List[Person] containing people whose name begins with 'A'" in {
            val people = repository.peopleMatching("A")
            
            people must have size(1)
            people(0).name must beEqualTo("Age Mooij")
        }

        "return None when retrieving a Person by an non-existing id" in {
            repository.person(4242) must beNone
        }
        
        "return Some[Person] when retrieving a Person by an existing name or id" in {
            val person1 = repository.personByName("Freek Wielstra")
            
            person1 must beSome
            
            val person2 = repository.person(person1.get.id)
            
            person2 must beSome
            person1.get must beEqualTo(person2.get)
            person1.get.skills.map(_.topic.name) must contain("Javascript", "Java").inOrder
            person1.get.interests.map(_.topic.name) must contain("Javascript", "Java").inOrder
        }
        
        "return Some[Person] when retrieving a Person by an existing username or id" in {
            val person1 = repository.personByUsername("fwielstra")
            
            person1 must beSome
            
            val person2 = repository.person(person1.get.id)
            
            person2 must beSome
            person1.get must beEqualTo(person2.get)
            person1.get.skills.map(_.topic.name) must contain("Javascript", "Java").inOrder
            person1.get.interests.map(_.topic.name) must contain("Javascript", "Java").inOrder
        }
        
        "return None when retrieving a Person by an non-existing username" in {
            repository.personByUsername("gbossuyt") must beNone
        }
        
        "return None when retrieving a Person by an non-existing name" in {
            repository.personByName("Harry Potter") must beNone
        }
    }
    
    "A Neo4jRepository loaded with topics from the standard test data set" should {
        "return all topics from the database" in {
            val topics = repository.topics
            
            topics must have size(8)
            topics.map(_.name) must contain("Scala", "Haskell", "Java", "Javascript", "Android", "iOS", "Neo4j", "MongoDB")
        }
        
        "return an empty List[Topic] for topics whose name begins with 'Z'" in {
            repository.topicsMatching("Z") must have size(0)
        }
        
        "return a List[Topic] containing topics whose name begins with 'Java'" in {
            val topics = repository.topicsMatching("Java")
            
            topics must have size(2)
            topics.map(_.name) must contain("Java", "Javascript")
        }

        "return None when retrieving a Topic by an non-existing id" in {
            repository.topic(4242) must beNone
        }
        
        "return Some[Topic] when retrieving a Topic by an existing id" in {
            val haskell1 = repository.topicNamed("Haskell").get
            val haskell2 = repository.topic(haskell1.id)
            
            haskell2 must beSome
            haskell1 must beEqualTo(haskell2.get)
        }
        
        "return None when retrieving a Topic by an non-existing name" in {
            repository.topicNamed("Sword fighting") must beNone
        }

        "return 0 people skilled at Android" in {
            val android = repository.topicNamed("Android").get
            
            repository.skilledAt(android) must have size(0)
        }
        
        "return 1 person skilled at Neo4j" in {
            val neo4j = repository.topicNamed("Neo4j").get
            val skilledAtNeo4j = repository.skilledAt(neo4j).map{case(person, skill) => (person.name -> skill)}
            
            skilledAtNeo4j must have size(1)
            skilledAtNeo4j("Age Mooij") must beEqualTo(Skill(neo4j, 6))
        }
        
        "return 3 people skilled at Java" in {
            val java = repository.topicNamed("Java").get
            val skilledAtJava = repository.skilledAt(java).map{case(person, skill) => (person.name -> skill)}
            
            skilledAtJava must have size(3)
            skilledAtJava("Age Mooij") must beEqualTo(Skill(java, 10))
            skilledAtJava("Bram Neijt") must beEqualTo(Skill(java, 6))
            skilledAtJava("Freek Wielstra") must beEqualTo(Skill(java, 7))
            skilledAtJava.keys must contain("Age Mooij", "Freek Wielstra", "Bram Neijt").inOrder
        }
        
        "return 0 people interested in Android" in {
            val android = repository.topicNamed("Android").get
            
            repository.interestedIn(android) must have size(0)
        }
        
        "return 1 person interested in iOS" in {
            val iOS = repository.topicNamed("iOS").get
            val interestedInIOS = repository.interestedIn(iOS).map{case(person, interest) => (person.name -> interest)}
            
            interestedInIOS must have size(1)
            interestedInIOS("Age Mooij") must beEqualTo(Interest(iOS, 8))
        }
        
        "return 2 people interested in Javascript" in {
            val javascript = repository.topicNamed("Javascript").get
            val interestedInJavascript = repository.interestedIn(javascript).map{case(person, interest) => (person.name -> interest)}
            
            interestedInJavascript must have size(2)
            interestedInJavascript("Age Mooij") must beEqualTo(Interest(javascript, 8))
            interestedInJavascript("Freek Wielstra") must beEqualTo(Interest(javascript, 9))
            interestedInJavascript.keys must contain("Freek Wielstra", "Age Mooij").inOrder
        }
        
        // "return a Set[String] of all unique topic categories" in {
        //     val categories = repository.topicCategories
        //     
        //     categories must have size(3)
        //     categories must contain("Programming Language", "Database", "Mobile Platform")
        // }
        // 
        // "return 4 topics for category 'Programming Language'" in {
        //     val topics = repository.topicsInCategory("Programming Language")
        //     
        //     topics must have size(4)
        //     topics.map(_.name) must contain("Java", "Scala", "Haskell", "Javascript")
        // }
    }
}