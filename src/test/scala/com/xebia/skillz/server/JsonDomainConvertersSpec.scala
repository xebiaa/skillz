package com.xebia.skillz.server

import org.specs2.mutable._
import net.liftweb.json._

import com.xebia.skillz._
import JsonDomainConverters._


class JsonDomainConvertersSpec extends SpecificationWithJUnit {
    val java  = Topic(id = 1, name = "Java",  tags = List("Programming Language", "Object Oriented").map(string => Tag(name = string)))
    val scala = Topic(id = 2, name = "Scala", tags = List("Programming Language", "JVM-based", "Functional", "Object Oriented").map(string => Tag(name = string)))
    
    "A Person" should {
        "render to JSON with only the mandatory fields and default optional values" in {
            val person = Person(id = 42, name = "Age Mooij", username = "amooy", email = "amooij@xebia.com")
                
            compact(render(person)) must beEqualTo(
                """{"id":42,"name":"Age Mooij","email":"amooij@xebia.com","skills":[],"interests":[]}"""
            )
        }
        
        "render to JSON with all optional properties valued Some(..)" in {
            val person = Person(
                id = 42, name = "Age Mooij", username = "amooy", email = "amooij@xebia.com", 
                skype = Some("agemooij"),
                twitter = Some("agemooij"),
                github = Some("agemooij"),
                stackOverflow = Some("agemooij"), 
                imageUrl = Some("http://localhost:8080/assets/people/amooij.png"))
                
            compact(render(person)) must beEqualTo(
                """{"id":42,"name":"Age Mooij","email":"amooij@xebia.com",""" + 
                """"skype":"agemooij","twitter":"agemooij","github":"agemooij","stackOverflow":"agemooij",""" +
                """"imageUrl":"http://localhost:8080/assets/people/amooij.png",""" + 
                """"skills":[],"interests":[]}"""
            )
        }
        
        "render to JSON including his skills and interests in summarized form" in {
            val person = Person(
                id = 42, 
                name = "Age Mooij",
                username = "amooy",
                email = "amooij@xebia.com",
                twitter = Some("agemooij"),
                skills = List(Skill(scala, 7), Skill(java, 10)),
                interests = List(Interest(scala, 10)))
                
            compact(render(person)) must beEqualTo(
                """{""" +
                """"id":42,"name":"Age Mooij","email":"amooij@xebia.com","twitter":"agemooij",""" +
                """"skills":[{"id":2,"name":"Scala","level":7},{"id":1,"name":"Java","level":10}],""" +
                """"interests":[{"id":2,"name":"Scala","level":10}]""" +
                """}"""
            )
        }
    }
    
    "A List[Person]" should {
        "render to JSON with only basic properties and number of skills/interests (list size = 1)" in {
            val people = List(
                Person(id = 1234, name = "Age Mooij", username = "amooy", email = "amooij@xebia.com", 
                       skills = List(Skill(java, 10), Skill(scala, 8)), interests = List(Interest(scala, 10), Interest(java, 6))))
            
            compact(render(people)) must beEqualTo(
                """[{"id":1234,"name":"Age Mooij","email":"amooij@xebia.com","nrOfSkills":2,"nrOfInterests":2}]""")
        }
        
        "render to JSON with only basic properties (list size > 1)" in {
            val people = List(
                Person(id = 12345678910L, name = "Age Mooij", username = "amooy", email = "amooij@xebia.com", skills = List(Skill(java, 10)), interests = List(Interest(scala, 10), Interest(java, 6))),
                Person(id = 10987654321L, name = "Bram Neijt", username = "bneijt", email = "bneijt@xebia.com", interests = List(Interest(scala, 6), Interest(java, 6))),
                Person(id = 3, name = "Freek Wielstra", username = "fwielstra", email = "fwielstra@xebia.com", skills = List(Skill(java, 7)))
            )
            
            compact(render(people)) must beEqualTo(
                """[""" +
                """{"id":12345678910,"name":"Age Mooij","email":"amooij@xebia.com","nrOfSkills":1,"nrOfInterests":2},""" +
                """{"id":10987654321,"name":"Bram Neijt","email":"bneijt@xebia.com","nrOfSkills":0,"nrOfInterests":2},""" +
                """{"id":3,"name":"Freek Wielstra","email":"fwielstra@xebia.com","nrOfSkills":1,"nrOfInterests":0}""" +
                """]"""
            )
        }
        
        "render to JSON including non-None optional properties (list size > 1)" in {
            val people = List(
                Person(id = 12345678910L, name = "Age Mooij", username = "amooy", email = "amooij@xebia.com", twitter = Some("agemooij"), skills = List(Skill(java, 10))),
                Person(id = 10987654321L, name = "Bram Neijt", username = "bneijt", email = "bneijt@xebia.com", stackOverflow = Some("bneijt"), github = Some("bneijt")),
                Person(id = 3, name = "Freek Wielstra", username = "fwielstra", email = "fwielstra@xebia.com", skype = Some("fwielstra"), imageUrl = Some("http://bla.com/freek.jpg"))
            )
            
            compact(render(people)) must beEqualTo(
                """[""" +
                """{"id":12345678910,"name":"Age Mooij","email":"amooij@xebia.com","twitter":"agemooij","nrOfSkills":1,"nrOfInterests":0},""" +
                """{"id":10987654321,"name":"Bram Neijt","email":"bneijt@xebia.com","github":"bneijt","stackOverflow":"bneijt","nrOfSkills":0,"nrOfInterests":0},""" +
                """{"id":3,"name":"Freek Wielstra","email":"fwielstra@xebia.com","skype":"fwielstra","imageUrl":"http://bla.com/freek.jpg","nrOfSkills":0,"nrOfInterests":0}""" +
                """]"""
            )
        }
    }
    
    
    "A Topic in combination with maps of skilled and interested people" should {
        "render to JSON including information about those people and their skills/interests" in {
            import _root_.scala.collection.mutable.LinkedHashMap
            
            var age = Person(id = 11, name = "Age Mooij", username = "amooy", email = "amooij@xebia.com", twitter = Some("agemooij"), interests = List(Interest(scala, 9)))
            val freek = Person(id = 12, name = "Freek Wielstra", username = "fwielstra", email = "fwielstra@xebia.com", twitter = Some("fwielstra"), skills = List(Skill(scala, 7)))
            val skilledAt = LinkedHashMap(age -> Skill(java, 10), freek -> Skill(java, 7)).toMap
            val interestedIn = LinkedHashMap(age -> Interest(java, 6), freek -> Interest(java, 7)).toMap
            
            compact(render(topicToJValueWithAllProperties(java, skilledAt, interestedIn))) must be equalTo(
                """{""" +
                """"id":1,"name":"Java","tags":["Programming Language","Object Oriented"],""" +
                """"skilledAt":[{"id":11,"name":"Age Mooij","email":"amooij@xebia.com","level":10},{"id":12,"name":"Freek Wielstra","email":"fwielstra@xebia.com","level":7}],""" + 
                """"interestedIn":[{"id":11,"name":"Age Mooij","email":"amooij@xebia.com","level":6},{"id":12,"name":"Freek Wielstra","email":"fwielstra@xebia.com","level":7}]""" + 
                """}"""
            )
        }
    }
    
    "A List[Topic]" should {
        "render to JSON with only basic properties (list size = 1)" in {
            compact(render(List(java))) must beEqualTo("""[{"id":1,"name":"Java","tags":["Programming Language","Object Oriented"]}]""")
        }
        
        "render to JSON with only basic properties (list size > 1)" in {
            compact(render(List(scala, java))) must beEqualTo(
                """[""" + 
                """{"id":2,"name":"Scala","tags":["Programming Language","JVM-based","Functional","Object Oriented"]},""" + 
                """{"id":1,"name":"Java","tags":["Programming Language","Object Oriented"]}""" + 
                """]"""
            )
        }
    }
}